Dir.chdir File.dirname __FILE__

require 'psych'

yaml = Psych.safe_load(File.read 'dns-simple.yml', aliases: true)
data = RecursiveOpenStruct.new yaml, recurse_over_arrays: true

data.zones.each_pair do |domain, hosts|
  resource.digitalocean_domain(domain) do |r|
    r.name = domain
  end

  hosts.each_pair do |host, record_sets|
    record_sets = [record_sets] if record_sets.is_a?(String)

    record_sets.each_pair do |type, record_set|
      record_set = [record_set] if record_set.is_a?(String)

      record_set.each do |record|
        resource.digitalocean_record(domain, host, record) do |r|
          r.name = host
          r.domain = domain
          r.type = type
          r.value = record
        end
      end
    end
  end
end
