#!/usr/bin/env ruby

require 'bundler/inline'
require 'json'

gemfile do
  source 'https://rubygems.org'

  if File.exist?('.ruby-version')
    ruby File.read('.ruby-version').strip
  end

  gem 'recursive-open-struct'
end

module Rubyform; end

class Rubyform::Resource
  attr_reader :type, :id, :arguments

  def initialize type, id
    @id = id
    @type = type
    @arguments = {}
  end

  def method_missing method, *arguments
    method = method.to_s

    example = "Example: t.name = 'hello'"
    if !method.end_with? '='
      raise "Arguments are to be passed as assignment, got: #{method}. #{example}"
    elsif arguments.size > 1
      raise "Only one argument is expected in an assignment, got: #{arguments.size}"
    end

    name = method.chomp '='

    @arguments[name] = arguments[0]
  end
end

class Rubyform::ResourceBuilder
  attr_reader :resources

  def initialize
    @resources = []
  end

  def generate_id segments
    # https://www.terraform.io/docs/configuration/syntax.html#identifiers
    segments.join('__').gsub(/[^a-zA-Z0-9_-]/i, '_')
  end

  def method_missing name, *id_segments
    if id_segments.empty?
      raise "1 or more arguments expected, got 0. Example: resource.aws_route53_record('identifier') do |r| ... end"
    end

    id = generate_id id_segments

    resource = Rubyform::Resource.new name, id
    resources << resource
    yield resource
    resource
  end

  def to_h
    hash = {}

    resources.map do |resource|
      container = hash[resource.type] ||= {}
      unless container[resource.id].nil?
        raise "Found duplicate resource: #{resource.id}"
      end
      container[resource.id] = resource.arguments
    end

    {resource: hash}
  end
end

module Rubyform::Dsl
  def resource
    @resource_builder ||= Rubyform::ResourceBuilder.new
  end
end

include Rubyform::Dsl

ARGV.each do |arg|
  require_relative arg
end

puts JSON.pretty_generate(@resource_builder.to_h)
