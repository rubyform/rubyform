# Rubyform

Rubyform is a Ruby DSL for Terraform.
It allows you to write Terraform projects in Ruby instead of HCL.


# License

Copyright 2020 The Rubyform Authors.

Licensed under the [Apache License, Version 2.0](LICENSE).